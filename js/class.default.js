var controller = (function(parameters)
{ 	
 	//	Object
 	var _this 	 = {};

 	//	Object private variables
	var _private = {
		//	
		 
	};

	//	Object public variables
	this._public = {
		
	};


	//	Constructor method.
   	(function(){
    	
    	

    })();


    //
    this.method = function()
    {

    };

 
	//
	return (function(scope){
		//	pass any parameters needed for the initialising of the class.
		for(var key in parameters){
			if( scope._public.hasOwnProperty(key) ){
				scope._public[key] = parameters[key];
			}
		}

		//	run any constructors which are defined with 'double underscore.'
		for(var method in scope){
			if( typeof(scope[method]) == "function" && method.indexOf("__") === 0  ){
				//	run any binding constructor methods.
				scope[method]();
			}
		}
	
		return _this = scope;

	})(this);
});	


var con = new controller({variable : value});

con.method();