var lesBrown = (function(parameters)
{ 	
 	var _this 	 = {};
	var _private = {
		//	
		root : false,
	};

	// page global parameters
	this._public = {
		diameter 		: false,
		circumference	: false,
		//
		ring 			: 1,
		rings			: 5,
		//
		diameters  		: [],
		circumferences 	: [],	
	};

	//	run any controllers needed across the section.
	this._contruct = function()
	{
		//	Set Les Brown's CubeRoot value (2√3.141592653589793 = 2√1.772453850905516 = 1.33133536380039)
		_private.root = Math.sqrt(Math.sqrt(Math.PI));		
	}; 


	//	calcute the rings...
	this.calculate = function()
	{
		var diameters 		= this._public.diameters 		= [];
		var circumferences 	= this._public.circumferences 	= [];
		var diameter 		= this._public.diameter;
		var circumference 	= this._public.circumference;

		//
		if( circumference !== false ){
			diameter = (circumference / Math.PI);
		}

		//
		if( this._public.ring == this._public.rings ){
			//	reservse the values as we need an octave 'down'
			diameters.push(diameter);
			circumferences.push((diameter * Math.PI));	
			for(var r = 1; r < this._public.rings; r++){
				diameter = (parseFloat(diameter) / _private.root);
				diameters.push(diameter);
				circumferences.push(diameter * Math.PI);	
			}
		}else{
			diameters.push(diameter);
			circumferences.push((diameter * Math.PI));	
			for(var r = this._public.ring; r < this._public.rings; r++){
				//
				diameter = (parseFloat(diameter) * _private.root);
				//
				diameters.push(diameter);
				circumferences.push((diameter * Math.PI));					
			}	
		}
	};
			
	this.output = function(type)
	{
		this.calculate();
		//

		switch(type){

			case "rings" : 
				for(var i = 0; i < this._public.diameters.length; i++){

					
				}
			break;


			default :
				for(var i = 0; i < this._public.diameters.length; i++){

					var d = this._public.diameters[i];
					var c = this._public.circumferences[i];

					document.write((i + 1) +" : diameter "+  d.toFixed(6) +" - circum. "+ c.toFixed(6) +"<br />");
				}
				document.write("<br />");

			break;	
		}	
	};			


	//	get/set diameter.
	this.diameter = function(diameter)
	{
		if( typeof(diameter) !== "undefined" ) this._public.diameter = diameter;

		return this._public.diameter;
	};


	//	get/set circumference.
	this.circumference = function(circumference)
	{
		if( typeof(circumference) !== "undefined" ) this._public.circumference = circumference;

		return this._public.circumference;
	};


	//	get/set rings.
	this.rings = function(rings)
	{
		if( typeof(rings) !== "undefined" ) this._public.rings = rings;

		return this._public.rings;
	};


	//	get/set ring.
	this.ring = function(ring)
	{
		if( typeof(ring) !== "undefined" ) this._public.ring = ring;

		return this._public.ring;
	};
		
 
	//
	return (function(scope){
		//	pass any parameters needed for the initialising of the class.
		for(var key in parameters){
			if( scope._public.hasOwnProperty(key) ){
				scope._public[key] = parameters[key];
			}
		}

		//	run any constructors which are defined with 'underscore.'
		for(var method in scope){
			if( typeof(scope[method]) == "function" && method.indexOf("_") === 0  ){
				//	run any binding constructor methods.
				scope[method]();
			}
		}
	
		return _this = scope;

	})(this);
});	

// var lb = new lesBrown({circumference :1.57, ring : 1});
// lb.output();